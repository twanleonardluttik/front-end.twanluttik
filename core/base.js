import pulse from 'pulse-framework';
import colors from './helpers/colors';

export default new pulse.Library({
  data: {

  },
  actions: {
    initColors({ base }) {
      // document.documentElement.style.setProperty('--your-variable', '#YOURCOLOR');
      for(var index in colors) {
        // console.log(colors[index])
        document.documentElement.style.setProperty('--' + index, colors[index]);
      }
    }

  }
})