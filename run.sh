# Install node-modules
npm i

# Run build
npm run build

# Create the main folder
mkdir /var/www/twanluttik/

# Clear the website root folder
rm -r /var/www/twanluttik/*

# Copy the dist to the website root folder
cp -a ./dist/. /var/www/twanluttik/

